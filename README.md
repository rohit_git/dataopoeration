## DataOperation on .xlxs file

# create virtual env. -
```
  python3 -m venv envdata
```

# enter the project directory  -
```
pip3 install -r requirement.txt
```
# Run project'
python manage.py runserver 0:8000
  
# Scope:
  1. Upload the .xlsx file
  2. Store it in the server in the optimal way (assume the file can be as big as 100 MB)
  3. column field validations
  
# Rest Api
```
--api name - {{ip}}/api/upload_xlsx/
```

#need to pass data in form_data using postman

```
xlsxFile : Orders-With Nulls.xlsx
  ```




once you fire the api then generate
```
"OrderData.xlsx" file in your local system and ignore list data show in json  format with ignored reason
```

for the ignore list entry's, if value is missmatch
we also show the mismatch value and reason
```json
  {
            "orderid": 3,
            "orderqty": 6,
            "sales": 261.54,
            "shipmode": "Regular Air",
            "Profit": "-213.25-Value should be non zero",
            "unitPrice": 38.94,
            "orderdate": "2010-10-13",
            "CustomerName": "Muhammed MacIntyre",
            "customer_segment": "Small Business",
            "product_category": "Office Supplies"
        },
```

# you can also upload xlxs file  on s3 buket
Note - use your AWS authentication credentials

```
AWS_ACCESS_KEY_ID = "xxxxxxx"
AWS_SECRET_ACCESS_KEY = "xxxxxx"
AWS_BUCKET_NAME = "xxxxxxx"
AWS_HOST = "xxxxx"
S3_LOGO_DIR = "dataValidation"
content_type = ".xlxs"
import boto3
import datetime
import logging
logger = logging.getLogger(__name__)
s3_filename = f'{S3_LOGO_DIR}/logo_{str(datetime.datetime.now().timestamp())}.{content_type}'


 try:
    session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
    s3 = session.resource('s3')
    bucket = s3.Bucket(AWS_BUCKET_NAME)
    obj = bucket.put_object(Key=s3_filename, Body=file_data, ContentType=str(content_type), ACL='public-read', )
    image_url = f'https://{AWS_HOST}/{obj._bucket_name}/{obj.key}'
except Exception as e:
    image_url = ""
    logger.info(e)
    logger.info("###########....file not sent to s3 bucket")
return image_url
```

