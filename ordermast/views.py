import pandas as pd
from django.http import JsonResponse
from rest_framework import status
from rest_framework.status import HTTP_406_NOT_ACCEPTABLE
from rest_framework.views import APIView
from ordermast.core.xlxs_validations import DataValidation

class UploadXlSX(APIView,DataValidation):
    def post(self, request):
        self.success_context = dict()
        self.error_context = dict()

        self.xlxs_file = request.FILES.get("xlsxFile")
        if not self.xlxs_file:
            self.error_context["message"] = "required all fields"
            return JsonResponse(self.error_context, status=status.HTTP_400_BAD_REQUEST)
        if not str(self.xlxs_file).endswith('.xlsx'):
            self.error_context["message"] = "The file must be of the following type: .xlsx"
            return JsonResponse(self.error_context, status=HTTP_406_NOT_ACCEPTABLE)

        try:
            df = pd.read_excel(self.xlxs_file)
            df.columns = df.columns.str.replace(' ', '')

        except:
            self.error_context["message"] = "Empty xlsx File"
            return JsonResponse(self.error_context, status=HTTP_406_NOT_ACCEPTABLE)

        if len(df) == 0:
            self.error_context["message"] = "Empty xlsx File"
            return JsonResponse(self.error_context, status=HTTP_406_NOT_ACCEPTABLE)
        validations_date=DataValidation()
        data=validations_date.xlsx_file_validation(df)
        return data