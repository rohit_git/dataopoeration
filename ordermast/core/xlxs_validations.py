import pandas as pd
from django.http import JsonResponse
from rest_framework import status
import logging
logger = logging.getLogger(__name__)


error_msg1 = "Should not contain special characters except underscore"
error_msg2 = "value should be non zero, non negative"
error_msg3 = "value should be non zero, non negative"
error_msg4 = "Should contains only the following value(Regular Air | Delivery Truck | Express Air)"
error_msg5 = "Value should be non zero"

error_list = [error_msg1, error_msg2, error_msg3, error_msg4, error_msg5]


## Note - currently  i don,t have AWS credentials so i use the dummy credentials,i did the s3 buket upload  code,it will be worked

# AWS_ACCESS_KEY_ID = settings.AWS_CLOUD_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY = settings.AWS_CLOUD_SECRET_ACCESS_KEY
# AWS_HOST = settings.AWS_CLOUD_HOST
# AWS_BUCKET_NAME = settings.AWS_CLOUD_BUCKET_NAME


AWS_ACCESS_KEY_ID = "xxxxxxx"
AWS_SECRET_ACCESS_KEY = "xxxxxx"
AWS_BUCKET_NAME = "xxxxxxx"
AWS_HOST = "xxxxx"
S3_LOGO_DIR = "dataValidation"
content_type = ".xlxs"
import boto3
import datetime
s3_filename = f'{S3_LOGO_DIR}/logo_{str(datetime.datetime.now().timestamp())}.{content_type}'

# this class we are using for data validations and upload the .xlxs file on s3 buket
class DataValidation():
    def xlsx_file_validation(self, df):
        valid_list = []
        ignoredvalidation = []
        for val in df.itertuples():
            dictdata = dict()
            dictdata["orderid"] = val.OrderID if isinstance(val.OrderID, int) or str(val.OrderId) == "-" else str(val.OrderID) + "-" + error_msg1
            dictdata["validation1"] = val.OrderID if isinstance(val.OrderID, int) or str(val.OrderId) == "-" else error_msg1

            dictdata["orderqty"] = val.OrderQuantity if isinstance(val.OrderQuantity,int) or val.OrderQuantity > 0 else str(val.OrderQuantity) + "-" + error_msg2
            dictdata["validation2"] = val.OrderQuantity if isinstance(val.OrderQuantity,int) or val.OrderQuantity > 0 else error_msg2

            dictdata["sales"] = val.Sales if isinstance(val.Sales, int) or val.Sales > 0 else str(val.Sales) + "-"  + error_msg3
            dictdata["validation3"] = val.Sales if isinstance(val.Sales, int) or val.Sales > 0 else error_msg3

            dictdata["shipmode"] = val.ShipMode if val.ShipMode in ["Regular Air", "Delivery Truck", "Express Air"] else str(val.ShipMode) + "-"+ error_msg4
            dictdata["validation4"] = val.ShipMode if val.ShipMode in ["Regular Air", "Delivery Truck", "Express Air"] else error_msg4

            dictdata["Profit"] = val.Profit if isinstance(val.Profit, int) or val.Profit > 0 else str(val.Profit) + "-"  + error_msg5
            dictdata["validation5"] = val.Profit if isinstance(val.Profit, int) or val.Profit > 0 else error_msg5

            dictdata["unitPrice"] = val.UnitPrice if isinstance(val.UnitPrice, int) or val.UnitPrice > 0 else str(val.UnitPrice) + "-" + error_msg5
            dictdata["validation6"] = val.UnitPrice if isinstance(val.UnitPrice, int) or val.UnitPrice > 0 else error_msg5

            dictdata["orderdate"] = str(val.OrderDate.date())
            dictdata["CustomerName"] = val.CustomerName
            dictdata["customer_segment"] = val.CustomerSegment
            dictdata["product_category"] = val.ProductCategory
            check_validation = [w in error_list for w in dictdata.values()]
            if True in check_validation:
                del dictdata["validation1"]
                del dictdata["validation2"]
                del dictdata["validation3"]
                del dictdata["validation4"]
                del dictdata["validation5"]
                del dictdata["validation6"]
                ignoredvalidation.append(dictdata)
            else:
                del dictdata["validation1"]
                del dictdata["validation2"]
                del dictdata["validation3"]
                del dictdata["validation4"]
                del dictdata["validation6"]

                valid_list.append(dictdata)

        order_data = pd.DataFrame(valid_list)
        file_name = 'OrderData.xlsx'
        order_data.to_excel(file_name)

        ## following operations for upload xlxs file on s3 buket

        # upload_to_s3_helper(s3_filename, order_data, content_type)

        if ignoredvalidation != []:
            self.error_context = {}
            self.error_context["data"] = ignoredvalidation
            self.error_context["message"] = "ignore list records"
            return JsonResponse(self.error_context, status=status.HTTP_401_UNAUTHORIZED)
        else:
            self.success_response = {}
            self.success_response["message"] = "no data in ignore list"
            return JsonResponse(self.success_response, status=status.HTTP_200_OK)






def upload_to_s3_helper(s3_filename, file_data, content_type):
        try:
            session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            s3 = session.resource('s3')
            bucket = s3.Bucket(AWS_BUCKET_NAME)
            obj = bucket.put_object(Key=s3_filename, Body=file_data, ContentType=str(content_type), ACL='public-read', )
            image_url = f'https://{AWS_HOST}/{obj._bucket_name}/{obj.key}'
        except Exception as e:
            image_url = ""
            logger.info(e)
            logger.info("###########....file not sent to s3 bucket")
        return image_url